const currentTime = document.querySelector(".para");
const buttonParent = document.querySelector(".btn-container");

let seconds = 0;
let minutes = 0;
let hours = 0;
let timerId;

function displayTime(){
    currentTime.innerText = `${hours < 10 ? `0${hours}`: hours} : ${minutes < 10 ? `0${minutes}`: minutes} : ${seconds < 10 ? `0${seconds}`: seconds}`;    
}

function handleButtonClick(event){
    const button = event.target.name;
    if (button === "start"){
        timerId = setInterval(() => {
            seconds++;
            if (seconds>58){
                seconds = 0;
                minutes++;
            }
            if (minutes>58){
                minutes = 0;
                hours++;
            }
            displayTime();
        }, 1000);    
    }
    if (button === "stop"){
        clearInterval(timerId);
    }
    if (button === "reset"){
        seconds = 0;
        minutes = 0;
        hours = 0;
        displayTime();
    }    
}

buttonParent.addEventListener("click", handleButtonClick);